(function($, undefined) {

	//my task всем инпутам text, кроме первого, в значение value добавить jQuery.
	$myInput = $('input[type="text"]').not(":first-child");
	$myInput.val('jQuery');

	//default
	var $a = $('[href="#abc"]');
	var $li = $a.parents('ul').find('li:last-child');
	var $color = $li.css('color', '#008000')

	// sergii milkovsky task
	var $ul = $('#menu ul:last-child');
	var $li = $ul.find('li').not(':last').addClass('active');

	// alik task
	var $menu = $('#menu');
	var $p = $menu.find('p');
		$p.on('click', function() {
			$(this).css('color', '#c0c0c0');
		});

	//alexander levchenko task
	function random(min, max) {
	    return Math.floor(Math.random() * (max - min)) + min;
	}

	function randomColor() {
	   return 'rgb(' + random(0, 255) + ', ' + random(0, 255) + ', ' + random(0, 255) + ')';
	}

	var $label = $('label:contains("I was born")');
	$label.on('click', function() {
		$(this).css('color', randomColor);
	});

	//andrey ledyakin
	var $paragraph = $('.left + p');
		$paragraph.css('font-size', '35px');

	//ilia semenko
	var $searchInput = $('.search');
		$searchInput.find('input:last-child');

	//sergey ziatko
	var $labelParent = $('input[type="checkbox"]').parent().parent().find('label');
		$labelParent.parent().parent().find('input:checked').css('margin','10px')

	//aleksey chernyak
	var $p = $('div h5:first + p:contains("Nunc")');
		$p.css('color', '#ddd')

	//yulia yascun
	var $last = $('h1 + div :header:last');
		$last.css('cursor', 'pointer')
		$last.on('mouseenter', function() {
			$(this).css('color', 'grey')
		})
		$last.on('mouseleave', function() {
			$(this).css('color', 'red')
		})
		
})(jQuery);