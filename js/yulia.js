(function($, undefined) {
          /* default */
            $('a[href = "#abc"]').parents('ul').find('li').last().css('color', 'green');

          /* my */
            var $my = $('h1').siblings('div').find(':header').last();
            $my.mouseenter( function() { 
              $my.css('background-color', '#05dd05');
            });
            $my.mouseleave( function() { 
              $my.css('background-color', '#ffffff');
            }); 


          /* Alik */
            var $p = $('#menu').find('p').first();

            $p.click(function(){
              $p.css('font-size', '20px');
            });

          /* Sergii */
            var $li = $('#menu').find('ul:last').find('li');
            for (var i = 0; i < $li.length-1; i++){
              $li.eq(i).addClass('active');
            }


          /* Sasha */  
            var $label = $('label:contains("I was born")');
            var k = 1;
            $label.click(function() {
              k++;
              if ( k%3 == 0 ) { 
                $label.css(('color', '#aa0000')); 
              } else if ( k%2 == 0 ){
                $label.css('color', '#0000aa');
              } else {
                $label.css('color', '#aa00aa');                
              }
            });

           /* Serhii */
           var $input = $('input:checked').prev();
           for (var i = 0; i < $input.length; i++){
             if ($input.eq(i).parent().parent().has('label')){
              $input.eq(i).css({
                background: 'yellow',
                border: '3px red solid'
              });
             }
           }

          /* Illia  */
          $('.search').find('input:last').css('border', '2px solid #669977')

          /* Aleksei */
          var $div = $('h5:first +  p:contains("Nunc")').parent();
            $div.hover(function() {$div.css({
                background: 'pink'
              });
             }, function() {$div.css({
                background: 'white'
              });
            });

          /* Andrey */ 
           var $p2 = $('#profile_container').find('div:last').find('p:nth-child(2)');
           $p2.click(function(){ $p2.css('font-size', '22px');});

        })(jQuery)