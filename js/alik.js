//----------------- Мое задание: Чтобы при нажатии на <Р> менялся его цвет.

$('#menu').on('click', 'p', function () {
    $(this).css('color', 'red');
});

//----------------- задание Default

$('a[href="#abc"]').parents('ul').find('li').last().css('backgroundColor', 'red');

//----------------- Sergii Milkovskyi

$('#find').parent().children().not(':last').toggleClass('active');

//----------------- sashechka.lev@gmail.com

$('label:contains("I was born")').on('click', function () {
    var color = '#' + Math.random().toString(16).slice(2, 2 + Math.max(1, Math.min(6, 10)));
    $(this).css('backgroundColor', color);
});


//----------------- Serhii Ziatko

if ($('input[type=checkbox]:checked').parent().parent().find('label').length) {
    $('input[type=checkbox]:checked').css('margin', '20px');
}

//----------------- Illia Simenko

$('div[class=search]').find('input:last').css('borderColor', 'red');

//-----------------  ChernyakAleksei

$('h5:first + p:contains("Nunc")').parent('div').css('border', '1px solid blue');

//-----------------  Julia Yaschun

$('h1').parent().find('div :header:last').on('mousemove', function () {
    var color = '#' + Math.random().toString(16).slice(2, 2 + Math.max(1, Math.min(6, 10)));
    $(this).css('backgroundColor', color);
});

//-----------------  Andrey Ledyaykin

$('p[class=left] + p').on('mousemove', function () {
    var color = '#' + Math.random().toString(16).slice(2, 2 + Math.max(1, Math.min(6, 10)));
    $(this).css('backgroundColor', color);
});

