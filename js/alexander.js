(($, undefined) => {

  {
    // Illia Simenko
    let parentDivWithInputs = $('.search').has('input');
    let lastChild = parentDivWithInputs.find(':last-child');
    let lastInput = lastChild.find(':last-child');
  }

  {
    // Serhii Ziatko
    let a = $('input:checked').parent().parent().has('label');
    let input = a.children().children('input[type=checkbox]:checked').first();
  }

  {
    // ChernyakAleksei
    let parent = $('h5').eq(0).next('p:contains("Nunc")').parent();
  }

  {
    // Alik
    let firstParagraph = $('#menu p:first-child');
    
    firstParagraph.on('click', function (e) {
      e.preventDefault();
      $(this).css('color', '#FF00FF');
    });
  }

  {
    // Sergii Milkovskyi
    let ul = $('#menu ul:last-child');
    let lists = ul.find('li:not(:last-child)');
    // add classes
    lists.addClass('active');
  }

  {
    //Julia Yaschun 
    let p = $('h1').siblings('div').find(':header').last();
    
    p.on({
      mouseover: function (e) {
        e.preventDefault();
        $(this).css('background-color', '#FF00FF');
      },
      mouseleave: function (e) {
        e.preventDefault();
        $(this).css('background-color', 'transparent');
      }
    });
  }


  {
    // Andrey Ledyaykin
    let p = $('p.left').next('p');
    
    p.on('click', function () {
      $(this).css('font-size', '48px');
    })
  }

  {
    // my task (Alexander Levchenko)
    //generate random number in range [from, to]
    let randomInt = function (from, to) {
      if (from > to) {
        return 'The second argument must be greater then or equal to the first argument';
      }
      return Math.floor(Math.random() * (to - from + 1));
    }
    
    let label = $('label:contains("I was born")');
    
    label.on('click', function() {
      $(this).css('background-color', `rgb(${randomInt(0,255)},${randomInt(0,255)},${randomInt(0,255)})`);
    })
  }

})(jQuery)