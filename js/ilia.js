jQuery(function($) {
    // default
    $('a[href="#abc"]').parents('ul').children().filter(':last').css('color', '#426442');
    // illia simenko
    $('div.search:last > div:last').find('input:last').css('background-color', '#426442');
    // sashechka.lev
    $("label:contains('I was born')").on('click', (ev) => $(ev.target).css('background-color', `rgb(${randomByte()}, ${randomByte()}, ${randomByte()}`));
    // Serhii Ziatko
    if ($('input[type="checkbox"]').parent().parent().find('label').length) {
        $('input[type="checkbox"]:checked:first').addClass('checkkkkk');
    }
    // Julia Yaschun
    $('div h1').next().children().filter(':header:last').hover((ev) => $(ev.target).css('background-color', '#426442'),
                                                               (ev) => $(ev.target).css('background-color', 'white'));
    // Andrey Ledyaykin
    $('#profile_container').find('div > div:last').prev().find('p:first').next().css('background-color', '#426442');
    // Alik
    $('#menu > p').on('click', (ev) => $(ev.target).css('background-color', `rgb(${randomByte()}, ${randomByte()}, ${randomByte()}`));
    // Sergii Milkovskyi
    $('ul:last').children().not('li:last-child').addClass('active');
    // ChernyakAleksei
    $('div h5:first').next().filter('p:contains("Nunc")').css('background-color', '#426442');

});

const randomByte = () => parseInt(Math.floor(Math.random() * 256), 10);