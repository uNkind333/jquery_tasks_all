(function($, undefined) {
      //Default
      $('a[href = "#abc"]', 'ul').parents('ul').children().last().css('border', '1px solid green');
      
      //Alik Чтобы при нажатии на <Р> менялся его цвет.
      $('#menu').children().first().click(function() {
        $(this).css('color', 'red');
      })

      //Sashechka Найти Label с текстом 'I was born'
      //Чтобы при каждом нажатии менялся цвет.  
      $('label:contains("I was born")').click(function() {
        $(this).css('color', rndColor());
      })
      
      //Serhii Ziatko
      //Найти первый input [type=checkbox] который по дефолту checked, прародитель которого имеет label
      // $('input[type="checkbox"]').parent().parent().has('lable').attr('checked', 'false');
      console.dir($('*:has("label")').children().children('input[type="checkbox"]:checked').first());


      //Illia Simenko
      //Выбрать последний input в этом div-е
      $('div.search').last().find('input').last().css('border', '1px solid green');

      //Julia Yaschun 
      //Найти последний заголовок в диве, который лежит в диве рядом c заголовком h1. При наведении пусть меняется фон.
      $('h1').closest('div').find(':header').last().hover(function() {
        $(this).css('color', rndColor()); // 
      }, function() {
        $(this).css('color', 'black'); // 
      })

      //ChernyakAleksei
      //Найти див который содержит <p> с тектом Nunc, который находится после h5 который первый из списка h5 в этом диве
      $('h5').eq(0).next('p:contains("Nunc")').parent().css('border', '1px solid green');


      //Help Function
      function rndColor() {
        return 'rgb(' + rnd(0, 255) + ', ' + rnd(0, 255) + ', ' + rnd(0, 255) + ')';
      }

      function rnd(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
      }

    })(jQuery);
