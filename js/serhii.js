(function($, undefined) {
    const randomColor = () => `rgb(${ Math.floor( Math.random() * 256 ) }, ${ Math.floor( Math.random() * 256 ) }, ${ Math.floor( Math.random() * 256 ) })`;

    $(function(){
         //defolt task
        $( 'li', $('ul' ).has('a[href = "#abc"]')).last().css({'backgroundColor': 'yellow'});

        // My task
        // Найти первый input [type=checkbox] который по дефолту checked, прародитель которого имеет label
        $( $('*').parent( $('* input label')) ).children( 'input[type=checkbox]:checked:first' ).css({'backgroundColor': '#ff0000'});
        
        // ChernyakAleksei task
        // Найти див который содержит <p> с тектом Nunc, который находится после h5 который первый из списка h5 в этом диве
        $('h5:first + p:contains("Nunc")').parent().css({'backgroundColor': 'yellow'});
        
        // Illia Simenko task
        // Выбрать последний input в этом div-е(div.class="search" > div)
        $('.search div input:last').css({'backgroundColor': 'yellow'});

        // Sergii Milkovskyi task
        // Всем <li> кроме последнего добавить класс "active"
        $('li:not(:last)', $('ul').has('#find')).addClass('active');

        // Alik task
        // Чтобы при нажатии на <Р> менялся его цвет.
        $('p:first').css({'cursor': 'pointer'}).click( function(e){         
            $(this).css({'color': randomColor()});
        });

        // sashechka.lev task
        // Найти Label с текстом 'I was born' Чтобы при каждом нажатии менялся цвет.
        $('label:contains("I was born")').css({'cursor': 'pointer'}).click( function(e){         
            $(this).css({'color': randomColor()});
        });

        // Julia Yaschun task
        // Найти последний заголовок в диве, который лежит в диве рядом c заголовком h1. При наведении пусть меняется фон.
        $(':header:last' ,$('h1').siblings('div') ).hover(function (e) { 
           $(this).css({'color': randomColor() });
        });
        
        // Andrey Ledyaykin
        // Выбрать второй <p>. и делайте с ним все, что хотите. теперь он Ваш!!
        //$('div').has('p.left')
        $( 'p:nth-child(2)', $('div > p.left').parent()).hover(function (e) { 
           $(this).css({'color': randomColor() });
        });
        
    });
   
})(jQuery);